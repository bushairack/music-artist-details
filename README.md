## Music Artist
This app is created in Angular. This app called "music artist" which displays the data gatherd from the app in an "artist-details.component.html". The following requirements are implemented.

    * Load real data from server
    * Use HttpClient service
    * Use the data to create a nice looking artist details page.

    Displays the following information on the page:
        * Artist name
        * Formed year
        * Style
        * Website(with working link)
        * Biography in EN.
        * Country
        * "strArtistLogo" as a header image.
        * "strArtistFanart4" as a background image on the page.
        * All text should be easly readable.
1. First expansion contains:
    * Add an input field for the artist name and set a placeholder.
    * Handle whitespace in the artist name.
    *  Convert the artist name to lowercase.
    * Change the static for the api call to the value of the input field.
    * Add a button to start the api call.
    * Display the Artist details information based on the request artist in the input field.
2. Second expansion contains:
    * Use the artistid to get all albums for an artist by using the following api:
    theaudiodb.com/api/v1/json/{APIKEY}/album.php?i={artistid}
    * Display the albums with 
        * strAlbum
        * intYearReleased
        * intScore
    * strAlbumThumbBack as a picture on the right side of the webpage.
    * Display the artist information on the left side of the webpage.

Sample Output:

![alt text](musicArtistScreenShot.png)

