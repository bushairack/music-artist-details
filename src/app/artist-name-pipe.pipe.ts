import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'artistNamePipe',
})
export class ArtistNamePipePipe implements PipeTransform {
  transform(data: string): string {
    if (data === undefined) {
      return '';
    }
    return data.replace(' ', '_');
  }
}
