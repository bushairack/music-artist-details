import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ArtistDetailsComponent } from './artist-details/artist-details.component';
import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { FirstSentencePipe } from './first-sentence.pipe';
import {FormsModule} from '@angular/forms';
import { ArtistNamePipePipe } from './artist-name-pipe.pipe';

// @ts-ignore
@NgModule({
  declarations: [
    AppComponent,
    ArtistDetailsComponent,
    FirstSentencePipe,
    ArtistNamePipePipe
  ],
  imports: [
    BrowserModule, HttpClientModule, FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
