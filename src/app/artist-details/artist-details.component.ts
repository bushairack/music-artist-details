import { Component, Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ArtistNamePipePipe } from '../artist-name-pipe.pipe';
import { style } from '@angular/animations';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-artist-details',
  templateUrl: './artist-details.component.html',
  styleUrls: ['./artist-details.component.scss'],
})
export class ArtistDetailsComponent implements OnInit {
  constructor(private http: HttpClient) {}
  public details: any;
  public name: string;
  public albumName: string;
  private albumData;
  public score: boolean;
  getApiData(name: string): void {
    this.http
      .get(`https://www.theaudiodb.com/api/v1/json/1/search.php?s=${name}`)
      .subscribe((data: any) => {
        // console.log('debugging', data);
        this.http
          .get(
            `https://theaudiodb.com/api/v1/json/1/album.php?i=${data.artists[0].idArtist}`
          )
          .subscribe((albums: any) => {
            // console.log('debugging', albums);

            albums = albums.album.sort((a, b) => {
              // console.log(a.intYearReleased, b.intYearReleased);
              return b.intYearReleased - a.intYearReleased;
            });
            this.albumData = JSON.parse(JSON.stringify(albums));
            console.log('debugging', this.albumData);
            this.details = data.artists[0];
            this.details.albums = albums;

            // console.log('debugging', data, albums);
          });
      });
  }
  ngOnInit(): void {
    this.getApiData('Linkin Park');
  }
  getArtistInformation(name: string): void {
    console.log(name);
    name = ArtistNamePipePipe.prototype.transform(name);
    name = name.toLowerCase();
    console.log(name);
    this.getApiData(name);
  }
  albumSearch(): void {
    // albumName = ArtistNamePipePipe.prototype.transform(albumName);
    console.log('album Name', this.albumName);

    this.details.albums = this.albumData.filter((album) => {
      return album.strAlbum
        .toLowerCase()
        .includes(this.albumName.toLowerCase());
    });
    if (this.score === true) {
      this.details.albums = this.details.albums.filter((album) => {
        return album.intScore >= 7;
      });
    }
    // console.log(this.albumName);
  }
}
